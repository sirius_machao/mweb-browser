﻿using Cys_Model;
using System.Collections.Generic;

namespace Cys_Common.Settings
{
    public class FavoritesSetting
    {
        public List<TreeNode> FavoritesInfos { get; set; }
    }
}
