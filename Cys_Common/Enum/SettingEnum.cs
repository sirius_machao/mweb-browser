﻿namespace Cys_Common.Enum
{
    public enum SettingEnum
    {
        PersonalProfile,
        PrivacySearchAndServices,
        Appearance,
        Startup,
        NewTabPage,
        CookieAndSitePermissions,
        DefaultBrowser,
        SearchEngine,
        Download,
        FamilySafety,
        Language,
        Printer,
        System,
        ResetSettings,
        PhoneAndOtherSettings,
        AboutMEdge
    }

}
